# Contributor: George Hopkins <george-hopkins@null.net>
# Maintainer: George Hopkins <george-hopkins@null.net>
pkgname=imhex
pkgver=1.25.0
pkgrel=0
_patterns=de0e08916505c61fcfac2234f22134f17d14df70
pkgdesc="Hex editor for reverse engineers and programmers"
url="https://github.com/WerWolv/ImHex"
# armhf, armv7, x86: 32-bit not supported
# ppc64le: textrel in libromfs
arch="all !armhf !armv7 !ppc64le !x86"
license="GPL-2.0-or-later"
options="!check" # No testsuite
makedepends="
	capstone-dev
	cmake
	curl-dev
	file-dev
	fmt-dev
	freetype-dev
	glfw-dev
	glm-dev
	gtk+3.0-dev
	llvm-dev
	llvm-static
	mbedtls-dev
	nlohmann-json
	openssl-dev
	python3-dev
	samurai
	yara-dev
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/WerWolv/ImHex/releases/download/v$pkgver/Full.Sources.tar.gz
	imhex-patterns-$_patterns.tar.gz::https://github.com/WerWolv/ImHex-Patterns/archive/$_patterns.tar.gz
	"
builddir="$srcdir"/ImHex

prepare() {
	default_prepare

	mv "$srcdir"/ImHex-Patterns-$_patterns/ \
		ImHex-Patterns

	sed -i 's|-Werror||g' \
		cmake/build_helpers.cmake \
		lib/external/pattern_language/lib/CMakeLists.txt
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DUSE_SYSTEM_CURL=ON \
		-DUSE_SYSTEM_NLOHMANN_JSON=ON \
		-DUSE_SYSTEM_FMT=ON \
		-DUSE_SYSTEM_LLVM=ON \
		-DUSE_SYSTEM_YARA=ON \
		-DIMHEX_STRIP_RELEASE=OFF \
		-DIMHEX_OFFLINE_BUILD=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
dc5bdfe29dc344af871a6f87d861910be825fa10b84aae99380e5096bce9f9b1a8f668538a6b88169a8d245185d2e3cda28337d5202fe498c54c9f73e6fded51  imhex-1.25.0.tar.gz
8478ddbf677caecba461edb69660284473d6aeb11f2581253b1cbdd94ed93cf56e542dfcc8da6540797bd311e7a4a7ebfb0b3e82a2c5cdacaf9c90d1aceb2132  imhex-patterns-de0e08916505c61fcfac2234f22134f17d14df70.tar.gz
"
