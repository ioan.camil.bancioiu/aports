# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kplotting
pkgver=5.100.0
pkgrel=0
pkgdesc="Lightweight plotting framework"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev samurai"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kplotting-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
e5b404b56b76cd60e2f077cdd1c0edf6232c154731e963744bfd09d54b5d81bc65e92f2508f43b7ae60cf353f829e0964afd8d57d79a5120659fb22c92042fb0  kplotting-5.100.0.tar.xz
"
