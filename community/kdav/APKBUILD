# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdav
pkgver=5.100.0
pkgrel=0
pkgdesc="A DAV protocol implementation with KJobs"
url="https://community.kde.org/Frameworks"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
license="LGPL-2.0-or-later"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	qt5-qtxmlpatterns-dev
	samurai
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdav-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4210b65bfdf22f07a68dc6446752bcaa4f3f0c7aafe55ae6d23969fa1fed863151541d1e05c645fc5f8c982f44976a48ca2fa6097044780d4fd31a9e16c8a21d  kdav-5.100.0.tar.xz
"
