# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=just
pkgver=1.8.0
pkgrel=0
pkgdesc="Just a command runner"
url="https://github.com/casey/just"
# riscv64: rust currently broken on this arch
# s390x: blocked by nix crate
arch="all !riscv64 !s390x"
license="CC0-1.0"
checkdepends="bash fzf"
makedepends="cargo"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/casey/just/archive/$pkgver/just-$pkgver.tar.gz"

export CARGO_PROFILE_RELEASE_OPT_LEVEL="z"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --release --frozen
}

check() {
	# Skipped tests are somehow broken.
	cargo test --frozen -- \
		--skip choose::default \
		--skip edit::editor_precedence \
		--skip choose::multiple_recipes \
		--skip functions::env_var_functions
}

package() {
	cargo install --locked --offline --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*

	install -D -m 644 completions/just.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -D -m 644 completions/just.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -D -m 644 completions/just.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
b88177ba056689fb947a094eb5a23e3fbb885cb7f12b9711cc09d3b931b2e132f0518563f213f442ea156b44e93f0ffff18cc76c4f3ce8da95f0a57e6cfe3f0b  just-1.8.0.tar.gz
"
