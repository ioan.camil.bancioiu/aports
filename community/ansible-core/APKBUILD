# Contributor: Sean McAvoy <seanmcavoy@gmail.com>
# Maintainer: Sean McAvoy <seanmcavoy@gmail.com>
pkgname=ansible-core
pkgver=2.14.0
pkgrel=0
pkgdesc="core components of ansible: A configuration-management, deployment, task-execution, and multinode orchestration framework"
url="https://ansible.com"
options="!check" # for now
arch="noarch"
license="GPL-3.0-or-later"
depends="python3
	py3-jinja2
	py3-packaging
	py3-yaml
	py3-cryptography
	py3-paramiko
	py3-resolvelib<0.8.2
	py3-resolvelib>=0.5.4"
makedepends="py3-setuptools"
subpackages="$pkgname-doc"
source="https://pypi.python.org/packages/source/a/ansible-core/ansible-core-$pkgver.tar.gz"

replaces="ansible-base"

build() {
	python3 setup.py build
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname/examples/
	cp -r examples/* \
	"$pkgdir"/usr/share/doc/$pkgname/examples/
	install -m644 README.rst "$pkgdir"/usr/share/doc/$pkgname

	mkdir -p "$pkgdir"/usr/share/man/
	local man
	for man in ./docs/man/man?/*.?; do
		install -Dm644 "$man" \
			"$pkgdir"/usr/share/man/man${man##*.}/${man##*/}
	done
}

sha512sums="
09e5b45608f7e1f341100dd89a6277a71a5ff0a45244cd6862a10d0f35a993c3cf649a5f9936ac726f7e903269f98a975804418516e602b8dd983bfca23c66cb  ansible-core-2.14.0.tar.gz
"
